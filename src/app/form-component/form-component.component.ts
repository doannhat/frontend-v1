import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form-component',
  templateUrl: './form-component.component.html',
  styleUrls: ['./form-component.component.css']
})
export class FormComponentComponent implements OnInit {
  httpOptions = {
    headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Methods': 'POST',
        'Access-Control-Allow-Headers': 'Content-Type, Authorization',
    }
};
  formShipment: FormGroup;
  baseUrl = 'http://localhost:8081';
  shipmentRequest;
  messenger = '';
  loadingForm = false;
  returnUrl = '';

  constructor(private http: HttpClient, private router: Router, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.formShipment = this.formBuilder.group({
      sender_name: ['La Redoute', [Validators.required, Validators.email]],
      sender_email: ['laredoute@example.com', [Validators.required]],
      sender_phone: ['07 12 34 56 78', [Validators.required]],
      sender_address: ['Boulevard de Lafayette', [Validators.required]],
      sender_locality: ['Paris', [Validators.required]],
      sender_postal_code: ['75001', [Validators.required]],
      sender_country_code: ['FR', [Validators.required]],
      receiver_name: ['Marquise de Pompadou', [Validators.required]],
      receiver_email: ['marquise-de-pompadou@example.com', [Validators.required]],
      receiver_phone: ['07 98 76 54 32', [Validators.required]],
      receiver_address: ['175 Rue de Rome', [Validators.required]],
      receiver_locality: ['Marseille', [Validators.required]],
      receiver_postal_code: ['13006', [Validators.required]],
      receiver_country_code: ['FR', [Validators.required]],
      height: [10, [Validators.required]],
      width: [10, [Validators.required]],
      length: [10, [Validators.required]],
      dimension_unit: ['cm', [Validators.required]],
      amount: [100],
      weight_unit: ['g', [Validators.required]]
    });
  }

  getQuote(form) {
    const requestBody = form.value;
    let body = this.getBody(requestBody);
    const url = this.baseUrl + '/client/getquote';
    this.http.post(url, body, this.httpOptions).subscribe(res => {
      // let data: any;
      // data = res.data;
      // if (data.cost > 0) {
      //   this.messenger = 'Register shipment success';
      // } else {
      //   this.messenger = 'Register shipment Error';
      // }
      // setTimeout(() => {
      //   this.loadingForm = false;
      //   // this.router.navigate([this.returnUrl]);
      // }, 200);
      console.log(1);
    });
  }

  getBody(requestBody) {
    this.shipmentRequest = {
      origin: {
        contact: {
          name: requestBody.sender_name,
          email: requestBody.sender_email,
          phone: requestBody.sender_phone
        },
        address: {
          country_code: requestBody.sender_country_code,
          locality: requestBody.locality,
          postal_code: requestBody.postal_code,
          address_line1: requestBody.sender_address
        }
      },
      destination: {
        contact: {
          name: requestBody.receiver_name,
          email: requestBody.receiver_email,
          phone: requestBody.receiver_phone
        },
        address: {
          country_code: requestBody.receiver_country_code,
          locality: requestBody.receiver_locality,
          postal_code: requestBody.postal_code,
          address_line1: requestBody.receiver_address
        }
      },
      package: {
        dimensions: {
          height: requestBody.height,
          width: requestBody.width,
          length: requestBody.length,
          unit: requestBody.dimension_unit
        },
        grossWeight: {
          amount: requestBody.amount,
          unit: requestBody.weight_unit,
        }
      }
    };
  }

}
